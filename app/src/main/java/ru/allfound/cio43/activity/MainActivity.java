package ru.allfound.cio43.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import ru.allfound.cio43.R;
import ru.allfound.cio43.adapters.RegistrationCardRecyclerAdapter;
import ru.allfound.cio43.models.ClientCard;
import ru.allfound.cio43.models.RegistrationCard;
import ru.allfound.cio43.services.DatabaseHandler;
import ru.allfound.cio43.services.FTPClientFunctions;
import ru.allfound.cio43.services.FileSystemFunctions;
import ru.allfound.cio43.services.IFTPsettings;
import ru.allfound.cio43.tools.Utils;
import ru.allfound.cio43.tools.XmlParser;

public class MainActivity extends AppCompatActivity implements IFTPsettings {

    private static final int ZXING_CAMERA_PERMISSION = 1;
    private Class<?> mClss;
    SpotsDialog spotsDialogUpload;
    private RecyclerView recyclerView;

    private ArrayList<RegistrationCard> registrationCards;
    RegistrationCard registrationCard;
    private DatabaseHandler databaseHandler;
    FTPClientFunctions ftpFunctions;
    String fileName;

    private static final String TAG = "MainActivity";

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        setupToolbar();
        registrationCards = new ArrayList<>();
        databaseHandler = new DatabaseHandler(getApplicationContext());

        ftpFunctions = new FTPClientFunctions();
        ftpFunctions.checkInternetConnection(this);

        Intent intent = getIntent();
        String message = intent.getStringExtra("msg");
        if (message != null) {
            //Toast.makeText(this, "Регистрация: = " + message, Toast.LENGTH_SHORT).show();
            takeRegistrationData(message);
            intent.removeExtra("msg");
        }

        AsyncLoadFromBD asyncLoadFromBD = new AsyncLoadFromBD();
        asyncLoadFromBD.execute();
    }

    private class AsyncUpload2FTP extends AsyncTask<Void, Integer, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Boolean result = false;
            if (ftpFunctions.ftpConnect(FTP_HOST, FTP_USERNAME, FTP_PASSWORD, FTP_PORT)) {
                Log.d(TAG, "FTP connecting!");
                if (ftpFunctions.ftpUpload(fileName, fileName, " ", getApplicationContext())) {
                    databaseHandler.addRegistrationCard(registrationCard);
                    registrationCards = (ArrayList<RegistrationCard>) databaseHandler.fetchRegistrationCard();
                    result = true;
                }
                ftpFunctions.ftpDisconnect();
            }
            return result;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            setupRecyclerView();
            spotsDialogUpload.dismiss();
            if (result) {
                AlertDialog alertDialogOk = createAlertDialogOk();
                alertDialogOk.show();
            } else {
                AlertDialog alertDialogDismiss = createAlertDialogDismiss();
                alertDialogDismiss.show();
            }
        }
    }

    private void takeRegistrationData(String message) {
        Utils utils = new Utils(getApplicationContext());
        XmlParser xmlParser = new XmlParser();
        FileSystemFunctions fileSystemFunctions = new FileSystemFunctions(getApplicationContext());
        // 1) input data parsing
        registrationCard = xmlParser.parserRegistrationData(message);
        if (registrationCard == null) {
            Toast.makeText(this, "Данные не корректные! Зарегистрируйтесь еще раз!", Toast.LENGTH_SHORT).show();
            return;
        }
        registrationCard.setRegDate(utils.takeCurrentDate());
        registrationCard.setRegTime(utils.takeCurrentTime());
        // 2) create XML data
        ClientCard clientCard = utils.getClientCard();
        String registrationDataXml = utils.createRegistrationDataXml(clientCard, registrationCard);
        // 3) data save to file
        fileName = utils.generateFileName(clientCard, registrationCard);
        fileSystemFunctions.writeFile(fileName, registrationDataXml);
        // 4) upload to FTP and save to DB
        spotsDialogUpload = new SpotsDialog(this, "Идет передача данных...");
        spotsDialogUpload.show();
        AsyncUpload2FTP asyncUpload2FTP = new AsyncUpload2FTP();
        asyncUpload2FTP.execute();
    }

    private AlertDialog createAlertDialogOk() {
        AlertDialog.Builder adb = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        adb.setTitle("Регистрация");
        adb.setIcon(android.R.drawable.ic_dialog_info);
        adb.setMessage("Регистрация прошла успешно!");
        adb.setPositiveButton("OK", null);
        return adb.create();
    }

    private AlertDialog createAlertDialogDismiss() {
        AlertDialog.Builder adb = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        adb.setTitle("Ошибка регистрации");
        adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setMessage("Зарегистрируйтесь еще раз!");
        adb.setPositiveButton("OK", null);
        return adb.create();
    }

    private class AsyncLoadFromBD extends AsyncTask<Void, Integer, String> {
        @Override
        protected String doInBackground(Void... params) {
            String result = "";
            registrationCards = (ArrayList<RegistrationCard>) databaseHandler.fetchRegistrationCard();
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            setupRecyclerView();
        }
    }

    private void setupRecyclerView() {
        RegistrationCardRecyclerAdapter registrationCardRecyclerAdapter = new RegistrationCardRecyclerAdapter(registrationCards);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(registrationCardRecyclerAdapter);
    }

    public void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void launchSimpleActivity(View v) {
        launchActivity(SimpleScannerActivity.class);
    }

    public void launchActivity(Class<?> clss) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            mClss = clss;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, ZXING_CAMERA_PERMISSION);
        } else {
            Intent intent = new Intent(this, clss);
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ZXING_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(mClss != null) {
                        Intent intent = new Intent(this, mClss);
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(this, "Please grant camera permission to use the QR Scanner", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }
}