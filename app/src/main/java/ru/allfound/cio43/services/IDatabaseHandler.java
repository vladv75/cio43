package ru.allfound.cio43.services;

/**
 * Created by vvv on 20.05.16.
 */
public interface IDatabaseHandler {
    static final int DATABASE_VERSION = 1;
    static final String DATABASE_NAME = "REGISTRATION_DB";
    static final String TABLE_REGISTRATIONS = "registrations";

    static final String KEY_ID = "id";
    static final String KEY_EVENT = "event";
    static final String KEY_DESCRIPTION = "description";
    static final String KEY_DATE = "date";
    static final String KEY_TIME = "time";
    static final String KEY_EMAIL = "eMail";
    static final String KEY_PHONE = "phoneNumber";
    static final String KEY_WEB = "web";
    static final String KEY_REGDATE = "regDate";
    static final String KEY_REGTIME = "regTime";

    String CREATE_PURCHASES_TABLE = "CREATE TABLE "
            + TABLE_REGISTRATIONS + "("
            + KEY_ID + " integer primary key autoincrement,"
            + KEY_EVENT + " TEXT, "
            + KEY_DESCRIPTION + " TEXT, "
            + KEY_DATE + " TEXT, "
            + KEY_TIME + " TEXT, "
            + KEY_EMAIL + " TEXT, "
            + KEY_PHONE + " TEXT, "
            + KEY_WEB + " TEXT, "
            + KEY_REGDATE + " TEXT, "
            + KEY_REGTIME + " TEXT" + ")";
}
