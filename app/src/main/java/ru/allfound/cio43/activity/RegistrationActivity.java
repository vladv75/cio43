package ru.allfound.cio43.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import ru.allfound.cio43.R;
import ru.allfound.cio43.models.ClientCard;
import ru.allfound.cio43.tools.Utils;

/**
 * Created by vvv on 19.05.16.
 */
public class RegistrationActivity extends AppCompatActivity implements Validator.ValidationListener {

    @NotEmpty EditText editTextFirstName;
    @NotEmpty EditText editTextLastName;
    @NotEmpty EditText editTextMiddleName;
    @NotEmpty EditText editTextEmail;
    @NotEmpty EditText editTextPhone;
    @NotEmpty EditText editTextPosition;
    @NotEmpty EditText editTextOrganisation;

    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        editTextFirstName = (EditText) findViewById(R.id.ETFirstName);
        editTextLastName = (EditText) findViewById(R.id.ETLastName);
        editTextMiddleName = (EditText) findViewById(R.id.ETMiddleName);
        editTextEmail = (EditText) findViewById(R.id.ETEmail);
        editTextPhone = (EditText) findViewById(R.id.ETPhone);
        editTextPosition = (EditText) findViewById(R.id.ETPosition);
        editTextOrganisation = (EditText) findViewById(R.id.ETOrganisation);

        validator = new Validator(this);
        validator.setValidationListener(this);

        Button buttonRegistration = (Button) findViewById(R.id.registration_button);
        assert buttonRegistration != null;
        buttonRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

        /*
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if(ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.sign_up);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        */

        loadClientCard();
    }

    private void loadClientCard() {
        Utils utils = new Utils(getApplicationContext());
        ClientCard currentClientCard = utils.getClientCard();
        editTextFirstName.setText(currentClientCard.getFirstName());
        editTextLastName.setText(currentClientCard.getLastName());
        editTextMiddleName.setText(currentClientCard.getMiddleName());
        editTextEmail.setText(currentClientCard.geteMail());
        editTextPhone.setText(currentClientCard.getPhoneNumber());
        editTextPosition.setText(currentClientCard.getPosition());
        editTextOrganisation.setText(currentClientCard.getOrganisation());
    }

    private void saveClientCard() {
        Utils utils = new Utils(getApplicationContext());
        ClientCard currentClientCard = utils.getClientCard();
        currentClientCard.setFirstName(editTextFirstName.getText().toString());
        currentClientCard.setLastName(editTextLastName.getText().toString());
        currentClientCard.setMiddleName(editTextMiddleName.getText().toString());
        currentClientCard.seteMail(editTextEmail.getText().toString());
        currentClientCard.setPhoneNumber(editTextPhone.getText().toString());
        currentClientCard.setPosition(editTextPosition.getText().toString());
        currentClientCard.setOrganisation(editTextOrganisation.getText().toString());
        utils.setClientCard(currentClientCard);
    }

    @Override
    public void onValidationSucceeded() {
        saveClientCard();
        //Utils.setInfoRegistrationPassed(this, true);
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getApplicationContext());
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
