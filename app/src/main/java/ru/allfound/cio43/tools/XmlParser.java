package ru.allfound.cio43.tools;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;

import ru.allfound.cio43.models.RegistrationCard;

/**
 * Created by vvv on 19.05.16.
 */
public class XmlParser {

    public XmlParser() {
    }

    public RegistrationCard parserRegistrationData(String registrationData) {

        RegistrationCard registrationCard = new RegistrationCard();

        try {
            //registrationData = registrationData.replaceAll("\r", " ").replaceAll("\n", " ");
            // prepare data
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            // create parser
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(new StringReader(registrationData));

            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                switch (xpp.getEventType()) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        for (int i = 0; i < xpp.getAttributeCount(); i++) {
                            switch (i) {
                                case 0: registrationCard.setDescription(xpp.getAttributeValue(i));
                                    break;
                                case 1: registrationCard.setDate(xpp.getAttributeValue(i));
                                    break;
                                case 2: registrationCard.setTime(xpp.getAttributeValue(i));
                                    break;
                                case 3: registrationCard.seteMail(xpp.getAttributeValue(i));
                                    break;
                                case 4: registrationCard.setPhoneNumber(xpp.getAttributeValue(i));
                                    break;
                                case 5: registrationCard.setWeb(xpp.getAttributeValue(i));
                                    break;
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        registrationCard.setEvent(xpp.getText());
                        return registrationCard;
                    default:
                        break;
                }
                xpp.next();
            }
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

}
