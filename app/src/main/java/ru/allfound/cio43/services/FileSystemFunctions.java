package ru.allfound.cio43.services;

import android.content.Context;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by vvv on 21.05.16.
 */
public class FileSystemFunctions {

    private static final String LOG_TAG = "FileSystemFunctions";
    Context context;

    public FileSystemFunctions(Context context) {
        this.context = context;
    }

    public void writeFile(String filename, String data) {
        FileOutputStream outputStream;
        try {
            outputStream = context.openFileOutput(filename, context.MODE_PRIVATE);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream));
            bw.write(data);
            bw.close();
            Log.d(LOG_TAG, "File is save");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return {@code true} if this file was deleted
     */
    public boolean deleteFile(String filename) {
        File dir = context.getFilesDir();
        File file = new File(dir, filename);
        return file.delete();
    }
}
