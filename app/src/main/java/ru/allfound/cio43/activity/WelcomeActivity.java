package ru.allfound.cio43.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ru.allfound.cio43.R;
import ru.allfound.cio43.tools.Utils;

/**
 * Created by vvv on 19.05.16.
 */
public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Utils utils = new Utils(getApplicationContext());
        if(utils.getInfoRegistrationPassed()){
            Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
            startActivity(intent);
        }else{
            Intent intent = new Intent(WelcomeActivity.this, RegistrationActivity.class);
            startActivity(intent);
        }
    }
}

