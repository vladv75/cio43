package ru.allfound.cio43.services;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import ru.allfound.cio43.models.RegistrationCard;

/**
 * Created by vvv on 20.05.16.
 */
public class DatabaseHandler extends SQLiteOpenHelper implements IDatabaseHandler {

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_PURCHASES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + TABLE_REGISTRATIONS);
        onCreate(db);
    }

    public void addRegistrationCard(RegistrationCard registrationCard) {
        long rowID = -1;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        putRegistrationCard2DB(contentValues, registrationCard);
        try {
            rowID = db.insert(TABLE_REGISTRATIONS, null, contentValues);
        } catch (SQLiteConstraintException ex) {
            ex.printStackTrace();
        }
        registrationCard.setId(rowID);
        db.close();
    }

    // updates an existing registrationCard in the database
    public void update(RegistrationCard registrationCard) {
        ContentValues contentValues = new ContentValues();
        putRegistrationCard2DB(contentValues, registrationCard);
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_REGISTRATIONS, contentValues, KEY_ID + "=" + registrationCard.getId(), null);
        db.close();
    }

    public List<RegistrationCard> fetchRegistrationCard() {
        ArrayList<RegistrationCard> registrationCards = new ArrayList<RegistrationCard>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_REGISTRATIONS, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            RegistrationCard registrationCard = takeRegistrationCardFromDB(cursor);
            registrationCards.add(0, registrationCard);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return registrationCards;
    }

    public RegistrationCard findById(long id) {
        RegistrationCard registrationCard = null;
        String sql = "SELECT * FROM " + TABLE_REGISTRATIONS
                + " WHERE " + KEY_ID + " = ?";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, new String[] { id + "" });
        if (cursor.moveToNext()) {
            registrationCard = takeRegistrationCardFromDB(cursor);
        }
        cursor.close();
        db.close();
        return registrationCard;
    }

    public void deletePurchase(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_REGISTRATIONS, KEY_ID + "=" + id, null);
        db.close();
    }

    public void deletePurchases() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_REGISTRATIONS, null, null);
        close();
    }

    private RegistrationCard takeRegistrationCardFromDB(Cursor cursor) {
        RegistrationCard registrationCard = new RegistrationCard();
        registrationCard.setId(cursor.getLong(0));
        registrationCard.setEvent(cursor.getString(1));
        registrationCard.setDescription(cursor.getString(2));
        registrationCard.setDate(cursor.getString(3));
        registrationCard.setTime(cursor.getString(4));
        registrationCard.seteMail(cursor.getString(5));
        registrationCard.setPhoneNumber(cursor.getString(6));
        registrationCard.setWeb(cursor.getString(7));
        registrationCard.setRegDate(cursor.getString(8));
        registrationCard.setRegTime(cursor.getString(9));
        return registrationCard;
    }

    private void putRegistrationCard2DB(ContentValues contentValues, RegistrationCard registrationCard) {
        contentValues.put(KEY_EVENT, registrationCard.getEvent());
        contentValues.put(KEY_DESCRIPTION, registrationCard.getDescription());
        contentValues.put(KEY_DATE, registrationCard.getDate());
        contentValues.put(KEY_TIME, registrationCard.getTime());
        contentValues.put(KEY_EMAIL, registrationCard.geteMail());
        contentValues.put(KEY_PHONE, registrationCard.getPhoneNumber());
        contentValues.put(KEY_WEB, registrationCard.getWeb());
        contentValues.put(KEY_REGDATE, registrationCard.getRegDate());
        contentValues.put(KEY_REGTIME, registrationCard.getRegTime());
    }
}
