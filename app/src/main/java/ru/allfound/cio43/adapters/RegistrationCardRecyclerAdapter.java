package ru.allfound.cio43.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import ru.allfound.cio43.R;
import ru.allfound.cio43.models.RegistrationCard;

/**
 * Created by vvv on 20.05.16.
 */
public class RegistrationCardRecyclerAdapter
        extends RecyclerView.Adapter<RegistrationCardRecyclerAdapter.ViewHolder> {

    private List<RegistrationCard> registrationCards;
    private int position;

    public RegistrationCardRecyclerAdapter(List<RegistrationCard> registrationCards) {
        this.registrationCards = registrationCards;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.registration_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.tvEvent.setText(registrationCards.get(position).getEvent());
        holder.tvDescription.setText(registrationCards.get(position).getDescription());
        holder.tvData.setText(registrationCards.get(position).getDate());
        holder.tvTime.setText(registrationCards.get(position).getTime());
        holder.tveMail.setText(registrationCards.get(position).geteMail());
        holder.tvPhoneNumber.setText(registrationCards.get(position).getPhoneNumber());
        holder.tvWeb.setText(registrationCards.get(position).getWeb());
        holder.tvRegistration.setText("Регистрация: "
                + registrationCards.get(position).getRegDate()
                + " " + registrationCards.get(position).getRegTime());
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public int getItemCount() {
        return registrationCards.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvEvent;
        TextView tvDescription;
        TextView tvData;
        TextView tvTime;
        TextView tveMail;
        TextView tvPhoneNumber;
        TextView tvWeb;
        TextView tvRegistration;

        public ViewHolder(View view) {
            super(view);
            tvEvent = (TextView) view.findViewById(R.id.tvEvent);
            tvDescription = (TextView) view.findViewById(R.id.tvDescription);
            tvData = (TextView) view.findViewById(R.id.tvDate);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            tveMail = (TextView) view.findViewById(R.id.tveMail);
            tvPhoneNumber = (TextView) view.findViewById(R.id.tvPhoneNumber);
            tvWeb = (TextView) view.findViewById(R.id.tvWeb);
            tvRegistration = (TextView) view.findViewById(R.id.tvRegistration);
        }
    }
}
