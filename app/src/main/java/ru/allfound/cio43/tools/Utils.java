package ru.allfound.cio43.tools;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Calendar;

import ru.allfound.cio43.R;
import ru.allfound.cio43.models.ClientCard;
import ru.allfound.cio43.models.RegistrationCard;

/**
 * Created by vvv on 19.05.16.
 */
public class Utils {

    public String CURRENT_CLIENT_PREF;
    public static String INFO_PREF = "info_pref";

    public static final String COLUMN_CLIENT_FIRST_NAME = "first_name";
    public static final String COLUMN_CLIENT_LAST_NAME = "last_name";
    public static final String COLUMN_CLIENT_MIDDLE_NAME = "middle_name";
    public static final String COLUMN_CLIENT_EMAIL = "email";
    public static final String COLUMN_CLIENT_PHONE_NUMBER = "phone_number";
    public static final String COLUMN_CLIENT_POSITION = "position";
    public static final String COLUMN_CLIENT_ORGANISATION = "organisation";

    public static final String  INFO_REGISTRATION_PASSED = "registration_passed";

    private ClientCard currentClientCard;
    private static Boolean infoRegistrationPassed;
    Context context;

    public Utils(Context context) {
        this.context = context;
        CURRENT_CLIENT_PREF = context.getResources().getString(R.string.current_client_pref);
    }

    public String generateFileName(ClientCard clientCard, RegistrationCard registrationCard) {
        return  clientCard.geteMail() + "_" +
                registrationCard.getRegDate() + "_"  +
                registrationCard.getRegTime() + ".xml";
    }

    public String createRegistrationDataXml(ClientCard clientCard, RegistrationCard registrationCard) {
        StringBuilder stringBuilder = new StringBuilder();

        //special symbols
        stringBuilder.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>").append("\n");
        stringBuilder.append("<data>").append("\n");
        stringBuilder.append("<event").append("\n");
        //from RegistrationCard
        stringBuilder.append("regdate=\"").append(registrationCard.getRegDate()).append("\"").append("\n");
        stringBuilder.append("regtime=\"").append(registrationCard.getRegTime()).append("\"").append("\n");
        //from ClientCard
        stringBuilder.append("firstName=\"").append(clientCard.getFirstName()).append("\"").append("\n");
        stringBuilder.append("lastName=\"").append(clientCard.getLastName()).append("\"").append("\n");
        stringBuilder.append("middleName=\"").append(clientCard.getMiddleName()).append("\"").append("\n");
        stringBuilder.append("eMail=\"").append(clientCard.geteMail()).append("\"").append("\n");
        stringBuilder.append("phoneNumber=\"").append(clientCard.getPhoneNumber()).append("\"").append("\n");
        stringBuilder.append("position=\"").append(clientCard.getPosition()).append("\"").append("\n");
        stringBuilder.append("organisation=\"").append(clientCard.getOrganisation()).append("\"").append("\n");
        //from RegistrationCard
        stringBuilder.append("description=\"").append(registrationCard.getDescription()).append("\">").append("\n");
        stringBuilder.append(registrationCard.getEvent()).append("\n");
        //special symbols
        stringBuilder.append("</event>").append("\n");
        stringBuilder.append("</data>");

        return stringBuilder.toString();
    }

    public String takeCurrentTime() {
        Calendar now = Calendar.getInstance();
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);

        String hourString = Integer.toString(hour);
        hourString = hour < 10 ? "0" + hourString : "" + hourString;

        String minuteString = Integer.toString(minute);
        minuteString = minute < 10 ? "0" + minuteString : "" + minuteString;

        return hourString + ":" + minuteString;
    }

    public String takeCurrentDate() {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DAY_OF_MONTH);

        String monthString = Integer.toString(month);
        monthString = month < 10 ? "0" + monthString : "" + monthString;

        String dayString = Integer.toString(day);
        dayString = day < 10 ? "0" + dayString : "" + dayString;

        return dayString + "." + monthString + "." + year;
    }

    public ClientCard getClientCard() {
        if (currentClientCard == null) {
            SharedPreferences preferences =
                    context.getSharedPreferences(CURRENT_CLIENT_PREF, Context.MODE_PRIVATE);
            currentClientCard = new ClientCard();
            currentClientCard.setFirstName(preferences.getString(COLUMN_CLIENT_FIRST_NAME, ""));
            currentClientCard.setLastName(preferences.getString(COLUMN_CLIENT_LAST_NAME, ""));
            currentClientCard.setMiddleName(preferences.getString(COLUMN_CLIENT_MIDDLE_NAME, ""));
            currentClientCard.seteMail(preferences.getString(COLUMN_CLIENT_EMAIL, ""));
            currentClientCard.setPhoneNumber(preferences.getString(COLUMN_CLIENT_PHONE_NUMBER, ""));
            currentClientCard.setPosition(preferences.getString(COLUMN_CLIENT_POSITION, ""));
            currentClientCard.setOrganisation(preferences.getString(COLUMN_CLIENT_ORGANISATION, ""));
        }
        return currentClientCard;
    }

    public void setClientCard(ClientCard clientCard) {
        currentClientCard = clientCard;
        SharedPreferences preferences =
                context.getSharedPreferences(CURRENT_CLIENT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        if (currentClientCard != null) {
            editor.putString(COLUMN_CLIENT_FIRST_NAME, clientCard.getFirstName());
            editor.putString(COLUMN_CLIENT_LAST_NAME, clientCard.getLastName());
            editor.putString(COLUMN_CLIENT_MIDDLE_NAME, clientCard.getMiddleName());
            editor.putString(COLUMN_CLIENT_EMAIL, clientCard.geteMail());
            editor.putString(COLUMN_CLIENT_PHONE_NUMBER, clientCard.getPhoneNumber());
            editor.putString(COLUMN_CLIENT_POSITION, clientCard.getPosition());
            editor.putString(COLUMN_CLIENT_ORGANISATION, clientCard.getOrganisation());
        } else {
            editor.putInt("user_id", 0);
        }
        editor.commit();
    }

    public Boolean getInfoRegistrationPassed() {
        if (infoRegistrationPassed == null) {
            SharedPreferences preferences = context.getSharedPreferences(INFO_PREF, Context.MODE_PRIVATE);
            infoRegistrationPassed = preferences.getBoolean(INFO_REGISTRATION_PASSED, false);
        }
        return infoRegistrationPassed;
    }

    public void setInfoRegistrationPassed(Boolean infoRegistrationPassedValue) {
        infoRegistrationPassed = infoRegistrationPassedValue;
        SharedPreferences preferences = context.getSharedPreferences(INFO_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        if (infoRegistrationPassed != null) {
            editor.putBoolean(INFO_REGISTRATION_PASSED, infoRegistrationPassedValue);
        }
        editor.commit();
    }
}
