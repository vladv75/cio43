# CIO43 #

Android приложение для регистрации участников конференций и других мероприятий по штрих коду.

### Основные возможности: ###

* Создание карточки клуба ИТ-директоров
* Регистрация участника мероприятия клуба по штрих-коду
* Сохранение регистрационных данных на FTP-сервере клуба
* Получение дополнительной информации о мероприятии 

### Тестирование: ###

Для тестирования на http://qrcoder.ru/ создайте карточку по следующим данным:
<?xml version="1.0" encoding="utf-8"?><date><event description="Конгресс для ИТ-директоров бизнеса" date="26.05.2015" time="09:00" eMail="zemtsov.av@cio43.ru" phoneNumber="+79536720720" web="cio43.ru">V CIO Конгресс 7Холмов</event></date>

### Скрины приложения: ###
![11.png](https://bitbucket.org/repo/aMoAaA/images/2840424433-11.png) ![22.png](https://bitbucket.org/repo/aMoAaA/images/2859433193-22.png) ![33.png](https://bitbucket.org/repo/aMoAaA/images/3947975056-33.png) ![44.png](https://bitbucket.org/repo/aMoAaA/images/3680160644-44.png) ![55.png](https://bitbucket.org/repo/aMoAaA/images/206620963-55.png)