package ru.allfound.cio43.models;

/**
 * Created by vvv on 23.05.16.
 */
public class VisitorCard {
    private String regDate;
    private String regTime;
    private String firstName;
    private String lastName;
    private String middleName;
    private String eMail;
    private String phoneNumber;
    private String position;
    private String organisation;

    public VisitorCard() {
    }

    public VisitorCard(String regDate, String regTime, String firstName, String lastName,
                       String middleName, String eMail, String phoneNumber, String position,
                       String organisation) {
        this.regDate = regDate;
        this.regTime = regTime;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.eMail = eMail;
        this.phoneNumber = phoneNumber;
        this.position = position;
        this.organisation = organisation;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getRegTime() {
        return regTime;
    }

    public void setRegTime(String regTime) {
        this.regTime = regTime;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }
}
